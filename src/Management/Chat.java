
package Management;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Chat extends JFrame
{
    JTextField tf;
    JButton button;
    JTextArea ta;
    
   public Chat()
   {
      setTitle("Chat");
      setForeground(Color.WHITE);
      setBackground(Color.WHITE);
      getContentPane().setBackground(Color.WHITE);
      setLocation(300,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      getContentPane().setLayout(null);
      setVisible(true);
      
      tf = new JTextField();
      tf.setFont(new Font("New Times Roman",Font.PLAIN,20));
      tf.setForeground(new Color(255,255,255));
      setBackground(Color.DARK_GRAY);
      setBounds(12,12,344,38);
      getContentPane().add(tf);
      tf.setColumns(10);
      
      button = new JButton();
      button.setFont(new Font("Times New Roman",Font.ITALIC,20));
      button.setForeground(new Color(255,255,255));
      button.setBackground(Color.BLUE);
      button.setBounds(405,12,164,38);
      getContentPane().add(button);
      
      ta = new JTextArea();
      ta.setFont(new Font("Times New Roman",Font.ITALIC,20));
      ta.setForeground(Color.ORANGE);
      ta.setBackground(Color.DARK_GRAY);
      ta.setBounds(12,85,557,157);
      getContentPane().add(ta);
   }
   public static void main(String[] args)
   {
       new Chat();
   }
    
}
