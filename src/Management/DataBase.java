
package Management;

import static gra.MenuPanel.dataBase;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class DataBase 
{
    private Connection connection = null;
    private PreparedStatement pstmt;
    private ResultSet rs;
            
    public DataBase()
    {
    try 
    {
        Class.forName("com.mysql.jdbc.Driver");
    } 
    catch (ClassNotFoundException e) 
    {
        System.out.println("Where is your MySQL JDBC Driver?");
        e.printStackTrace();
        return;
    }
    
    System.out.println("MySQL JDBC Driver Registered!");
    

    try 
    {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gracze_statki","root", "");
    } 
    catch (SQLException e) 
    {
        System.out.println("Connection Failed! Check output console");
        e.printStackTrace();
        return;
    }

    if (connection != null) 
    {
        System.out.println("You made it, take control your database now!");
    } 
    else 
    {
        System.out.println("Failed to make connection!");
    }
    
    }
    
   public boolean addUserToDataBase(String name , char[] password)
    {       
        
        String checkSql = "select login from gracze where login=?";
        
        try 
        { 
            pstmt = connection.prepareStatement(checkSql);
            pstmt.setString(1, name);

            
            ResultSet result = pstmt.executeQuery();
            if(result.last())
            {
                JOptionPane.showMessageDialog(null,"W bazie istnieje juz gracz o takim loginie !"); 
                result.close();
                pstmt.close(); 
                return false;
            } 
           
            pstmt.close();
            
            String sql = "INSERT INTO GRACZE(login,password) VALUES(?,?)";
            
            pstmt =connection.prepareStatement(sql);
            
            pstmt.setString(1, name);
            pstmt.setString(2,String.valueOf(password));
            
            int wynik = pstmt.executeUpdate();
            System.out.println("Ilosc dodanych wierszy do bazy :" + wynik);

            JOptionPane.showMessageDialog(null,"Do bazy dodano gracza o loginie :" + name + "!"); 
            
            pstmt.close();
            result.close();
            
            return true;
            
        }

        
        catch (SQLException ex) 
        {
            ex.printStackTrace();
        }
        
       return false; 
    }
    public String loginWithAccount(String name , char[] password)
    {       
        
        String checkSql = "select login from gracze where login=? and password=?";
        
        
        try 
        { 
            
            pstmt = connection.prepareStatement(checkSql);
            
            pstmt.setString(1,name);
            pstmt.setString(2,String.valueOf(password));
            
            ResultSet result = pstmt.executeQuery();
            System.out.println(result.next());
            if(!result.last())
            {
                JOptionPane.showMessageDialog(null,"Nie udalo sie zalogowac z powodow: bledny login lub haslo badz dany uzytkownik jest juz zalogowany, ZAMYKANIE!");
                pstmt.close();
                result.close();
                return null;
            }          
            
        }
        catch (SQLException ex) 
        {
            ex.printStackTrace();
            return null;
        } 
       return name; 
    }
    public void setUserWon(String name)
    {
        String sql = "select wins from gracze where login=?";
        
        try {
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1,name);
            ResultSet result = pstmt.executeQuery();
            result.next();
            int wynik = result.getInt(1);
            System.out.println(wynik);
            wynik++;
            
            String update = "update gracze set wins=? where login=?";
            
            pstmt = connection.prepareStatement(update);
            pstmt.setInt(1, wynik);
            pstmt.setString(2, name);
            pstmt.executeUpdate();

        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
        }
        
    }
    public void setUserLost(String name)
    {
        String sql = "select losses from gracze where login=?";
        
        try {
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1,name);
            ResultSet result = pstmt.executeQuery();
            result.next();
            int wynik = result.getInt(1);
            System.out.println(wynik);
            wynik++;
            
            String update = "update gracze set losses=? where login=?";
            
            pstmt = connection.prepareStatement(update);
            pstmt.setInt(1, wynik);
            pstmt.setString(2, name);
            pstmt.executeUpdate();

        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
        }
        
    }
    public int getNumberOfRows()
    {
        int count=0;
        String scores = "select login,wins,losses from gracze";
        try 
        {
            pstmt = connection.prepareStatement(scores);
            ResultSet result = pstmt.executeQuery();
            while(result.next())
                count++;
        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
        }
        return count;
    }
    public DefaultTableModel createTableModel()
    {
        DefaultTableModel model = new DefaultTableModel(new String[]{"LOGIN","WINS","LOSSES"}, 0);
        String scores = "select login,wins,losses from gracze";
        
        try 
        {
            pstmt = connection.prepareStatement(scores);
            ResultSet result = pstmt.executeQuery();
            while(result.next())
            {
                    String login = result.getString("LOGIN");
                    String wins = result.getString("WINS");
                    String losses = result.getString("LOSSES");
                    model.addRow(new Object[]{login,wins,losses});
            }
        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
            return null;
        }
        
        return model;
    }
    
}
