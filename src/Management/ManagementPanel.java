package Management;

import interfacesAndOthers.PlayerObserver;
import java.util.Map;
import Serwer.Player;
import gra.GamePanel;
import interfacesAndOthers.Communicator;
import java.util.HashMap;



public class ManagementPanel implements PlayerObserver
{
    public static Map<Integer,Player> players = new HashMap<>();
    GamePanel gamePanel ;
    
    
    
    public ManagementPanel(int id , Communicator com)
    {
        gamePanel = new GamePanel(id,com);
       
    }
    
    @Override
    public boolean registerPlayer(Player player) {
       if(players.containsKey(player.getId()))
           return false;
       
       players.put(player.getId(), player);
       System.out.println( " Dodano gracza o id " + player.getId());
       
       
       return true;
    }

    @Override
    public void deregisterPlayer(int id) {
       players.remove(id);
    }


    @Override
    public boolean receiveShot(int id, short pozx, short pozy) {
        
         gamePanel.checkIfHit(pozx,pozy);
       
        return false;
      
    }@Override
    public void setReceivedInfoToPanel(int id,short pozx,short pozy,boolean enemyHit,boolean isDestroyed)
    {  
        if(enemyHit)
            gamePanel.enemyTab.addShot(pozx,pozy, 3,isDestroyed);
        else
            gamePanel.enemyTab.addShot(pozx,pozy, 2,false);
    }
    
    public void InfoToPanelUserHasAlreadyWonGame()
    {
        gamePanel.userHasAlreadyWongame();
    }
    
    @Override
    public void clearPlayers() {
       players.clear();
    }

    public void startMyTurn() {
        gamePanel.setStatusOfJPanel(true);
    }
    
    public void setAbilityToShoot()
    {
        gamePanel.setEnemySetAllShipsOnBoard(true);
    }

    public void endGame() {
        gamePanel.endGame();
    }

    public void InfoToPanelUserHasAlreadyLostGame() {
        gamePanel.userHasAlreadyLostgame();
    }


}
