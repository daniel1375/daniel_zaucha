
package Klient;

import static gra.MenuPanel.dataBase;
import interfacesAndOthers.Communicator;
import interfacesAndOthers.FrameType;
import interfacesAndOthers.GamerEvent;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JOptionPane;

public class Client implements Communicator
{
    private boolean looped = false;
    private boolean connection = false;
    
    private int clientId;
    private String clientLogin;
    
    private int serverId;
    private String serverLogin;
    private String login;
    
    private GamerEvent event;
    
    private Socket clientSocket = null;
    
    private Thread thread;
    
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    
    private boolean enemyHit = false;
    private boolean isHit = false;
    private boolean enemyShipDestroyed=false;
    
    private short pozx;
    private short pozy;
   
    
    
    
    
    public boolean connect(String host,int port,String login)
    {
        if(connection)
        {
             return false;
        }
        
        this.login=login;
        
        try
        {
            clientSocket = new Socket(host,port);
            inputStream = new DataInputStream(clientSocket.getInputStream());
            outputStream = new DataOutputStream(clientSocket.getOutputStream());
            
            outputStream.writeByte(FrameType.LOGIN_FRAME);
            outputStream.writeUTF(login);
            
            if(inputStream.readBoolean())
            {
                clientId = inputStream.readInt();
                clientLogin = login;
                
                connection = true;
                looped = true;
                
               JOptionPane.showMessageDialog(null,"Witaj klient ! Twoj login : " + login );
               
               JOptionPane.showMessageDialog(null,"Wypelnij swoja plansze wszystkimi statkami !" );
                
                runThread();
                
                return true;
            }
            else
                closeObjects();
        }
        catch(IOException ex)
        {
            JOptionPane.showMessageDialog(null," Wystapił błąd podczas łączenia sie z serwerem");
            closeObjects();
        }
        
        return false;
    }

    @Override
    public int getUserId() {
        return clientId;
    }

    @Override
    public String getUserLogin() {
        return clientLogin;
    }

    @Override
    public void setEvent(GamerEvent event) {
      this.event = event;
    }

    @Override
    public boolean sendSetShootToEnemy(int pozx, int pozy) {
        
        if(connection == false)
            return false;
        System.out.println(" CLIENT sendSetShootToEnemy , pozx:" + pozx + "  pozy : " + pozy);
        try 
        {
            outputStream.writeByte(FrameType.BOARD_CHANGE_STATUS_FRAME);
            outputStream.writeShort((short)pozx);
            outputStream.writeShort((short)pozy);
            
        } 
        catch (IOException ex) 
        {
           return false;
        }
        return true;
    }

    private synchronized void closeObjects()
    {
 
       closeObject(inputStream);
       closeObject(outputStream);
       closeObject(clientSocket);
       JOptionPane.showMessageDialog(null, "Zamykanie polaczenia z serwerem i zakonczenie gry");
       event.endGame();
       
    }
    

    private void runThread() {
        thread = new Thread( () -> {
            boolean error = false;
            try
            {
                while(looped)
                    makeIteration();
            }
            catch(IOException ex)
            {
                System.out.println( " Bład podczas make iteration w client");
                ex.printStackTrace();
                error = true;
            }
            finally
            {
                closeObjects();
                looped = false;
                connection = false;
                
                if(error)
                    event.connectionError();
            }
        });
        
           thread.start();
    }
    
    private boolean closeObject(Closeable object)
    {
        if(object == null)
            return false;
        
        try
        {
            object.close();
        }
        catch(IOException ex)
        {
            return false;
        }
        return true;
    }

    private boolean makeIteration() throws IOException {
       int frameType = inputStream.readByte();
       System.out.println( " make Iteration w Client ->  frame Type : " + frameType);
       
       
       
       switch( frameType )
       {
           case FrameType.LOGIN_FRAME:
               return interperetLoginFrame();
           case FrameType.LOGOUT_FRAME:
               return interpretLogoutFrame();
           case FrameType.BOARD_CHANGE_STATUS_FRAME:
               return interpretBoardChangeStatusFrame();
           case FrameType.SEND_INFO_TO_PANEL:
               return sendInfoToPanel();
           case FrameType.ENEMY_TURN:
                return startMyTurn();
           case FrameType.ENEMY_SET_ALL_SHIPS:
                return enemySetAllAships();
           case FrameType.ENEMY_HAS_ALREADY_WON_GAME:
               return interpretEnemyHasAlreadyWonGame();
               
           default:
               return false;
       }
       
    }

    private boolean interperetLoginFrame() throws IOException{
        serverId = inputStream.readInt();
        serverLogin = inputStream.readUTF();
        
        
        return true;
    }

    private boolean interpretLogoutFrame() throws IOException {
      clientId = inputStream.readInt();
      event.userLeft(clientId);
      
      return true;
    }

    private boolean interpretBoardChangeStatusFrame() throws IOException {
      serverId = inputStream.readInt();
      pozx = inputStream.readShort();
      pozy = inputStream.readShort();
       
      
      event.userReceivedShoot(getUserId(),pozx, pozy);
      
      startMyTurn();
      
      return true;
    }

    @Override
    public boolean sendInfoToPanel()
    {
        short my_pozx;
        short my_pozy;
        try
        {
            my_pozx = inputStream.readShort();
            my_pozy = inputStream.readShort();
            enemyHit = inputStream.readBoolean();
            enemyShipDestroyed = inputStream.readBoolean();
            
        }
        catch(IOException ex)
        {
            return false;
        }

       event.userSendsInfoToPanel(getUserId(),my_pozx,my_pozy,enemyHit,enemyShipDestroyed);
       if(enemyHit)
           startMyTurn();
       
        return enemyHit;
    }
    
    @Override
    public void isHit(boolean value,boolean isDestroyed) {
        
        isHit = value;
        System.out.println("Client isDestroyed = "+isDestroyed);
        try
        {
           outputStream.writeByte(FrameType.SEND_INFO_TO_PANEL);
           outputStream.writeShort((short)pozx);
           outputStream.writeShort((short)pozy);
           outputStream.writeBoolean(isHit);
           outputStream.writeBoolean(isDestroyed);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        
        
    }

    private boolean startMyTurn() {
        event.startMyTurn();
        return true;
    }

    private boolean enemySetAllAships() 
    {
        event.setAbilityToShoot();
        return true;
    }
    
    @Override
    public boolean allShipsOnBoard() {
        try 
        {
            outputStream.writeByte(FrameType.ENEMY_SET_ALL_SHIPS);
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void enemyWonGame() {
        try 
        {
            outputStream.writeByte(FrameType.ENEMY_HAS_ALREADY_WON_GAME);
        }
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
        event.userHasAlreadyLost();
    }

    private boolean interpretEnemyHasAlreadyWonGame() 
    {
       event.userHasAlreadyWon();
       return true;
    }



}
