
package Serwer;

public class Player 
{
    private final int id;
    private final String login;
    
    private short lastX;
    private short lastY;
    
    private short lastReceivedX;
    private short lastReceivedY;
    
    private boolean isHit = false;
    
    private boolean enemyHit = false;
    
    public Player(int id,String login)
    {
        this.id = id;
        this.login = login;
    }
    public int getId()
    {
        return id;
    }
    public String getLogin()
    {
        return login;
    }
    public void setShoot(int pozx,int pozy,boolean enemyHit)
    {
        lastX = (short)pozx;
        lastY = (short)pozy;
        this.enemyHit = enemyHit;
        System.out.println("Player setShot() , lastX="+lastX+"lastY"+lastY);
    }
    public void receiveShoot(int pozx,int pozy)
    {
        lastReceivedX = (short)pozx;
        lastReceivedY = (short)pozy;
    }
    public short getLastX()
    {
        System.out.println(" player od id =" + id + " LastX "+lastX);
        return lastX;
    }
    public short getLastY()
    {
        System.out.println(" player od id =" + id + " LastY "+lastY);
        return lastY;
    }
    public short getLastReceivedX()
    {
        return lastReceivedX;
    }
    public short getLastReceivedY()
    {
        return lastReceivedY;
    }
    public boolean getIsHit()
    {
        return isHit;
    }
    public void setIsHit(boolean value)
    {
        isHit = value;
    }
    public boolean getEnemyHit()
    {
        return enemyHit;
    }
    public void setEnemyHit(boolean value)
    {
        enemyHit = value;
    }
    
}
