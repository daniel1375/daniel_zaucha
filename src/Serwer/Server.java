
package Serwer;


import static gra.MenuPanel.dataBase;
import interfacesAndOthers.*;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;



public class Server implements Communicator{
    
    public static final String HOST = "localhost";
    public static final int PORT = 50007;
    
    private final Object locker = new Object();
    
    private boolean connection = false;
    private boolean looped = false;
    
    private GamerEvent event;
    
    private Player serverPlayer;
    private Player clientPlayer;
    
    private final int idServer;
    private final int idClient;
    private String login;
    
    private boolean isHit = false;
    
    
    private Thread thread;
    
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;
    
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;
    
    private short pozx;
    private short pozy;
    private boolean enemyHit = false;
    private boolean enemyShipDestroyed = false;
    

    public Server(int idServer , int idClient)
    {
        this.idServer = idServer;
        this.idClient = idClient;
    }
    
   
    
    public boolean runServer(int port , String login){
        
        if(connection)
            return false;
        
        this.login = login;
        
        try 
        {
            serverSocket = new ServerSocket(port);
        } catch (IOException ex) 
        {
            return false;
        }
        serverPlayer = new Player(idServer,login);
        
        while(true){
            
            thread = new Thread( () -> {
                while (looped) {
                    try 
                    {
                        clientSocket = serverSocket.accept();
                        inputStream = new DataInputStream(clientSocket.getInputStream());
                        outputStream = new DataOutputStream(clientSocket.getOutputStream());
                        while(looped && makeIteration());
                    } 
                    catch (IOException e)
                    {
                        event.userLeft(clientPlayer.getId());
                    } 
                    finally 
                    {
                        closeClient();
                    }
                }
            });
            looped = true;
            
            
            thread.start();
            connection = true;
            
            event.userLogged(serverPlayer);
            JOptionPane.showMessageDialog(null,"Witaj serwer ! Twoj login : " + login );
            JOptionPane.showMessageDialog(null,"Wypelnij swoja plansze wszystkimi statkami !" );
            
            return true;
            }
    }
    public void disconnect()
    {
        if(connection == false)
            return;
        
        looped = false;
        closeObjects();
        joinThread();
        connection = false;
        event.userLeft(serverPlayer.getId());
    }
    private boolean makeIteration() throws IOException
    {
        int frameType = inputStream.readByte();
        
        switch( frameType )
        {
            case FrameType.LOGIN_FRAME:
                return interpretLoginFrame();
            case FrameType.LOGOUT_FRAME:
                return interpretLogoutFrame();
            case FrameType.BOARD_CHANGE_STATUS_FRAME:
                return interpretBoardChangeStatusFrame();
            case FrameType.SEND_INFO_TO_PANEL:
               return sendInfoToPanel();
            case FrameType.ENEMY_TURN:
                return startMyTurn();
            case FrameType.ENEMY_SET_ALL_SHIPS:
                return enemySetAllAships();
           case FrameType.ENEMY_HAS_ALREADY_WON_GAME:
               return interpretEnemyHasAlreadyWonGame();
            default:
                return false;
        }
    }

    @Override
    public int getUserId() {
       return serverPlayer.getId();
    }

    @Override
    public String getUserLogin() {
       return serverPlayer.getLogin();
    }

    @Override
    public void setEvent(GamerEvent event) {
       this.event = event;
    }

    @Override
    public boolean sendSetShootToEnemy(int pozx, int pozy) {
        
       if(connection == false)
           return false;
       
       try
       {
           synchronized(locker)
           {
               if(outputStream != null)
               {
                   outputStream.writeByte(FrameType.BOARD_CHANGE_STATUS_FRAME);
                   outputStream.writeInt(serverPlayer.getId());
                   outputStream.writeShort((short)pozx);
                   outputStream.writeShort((short)pozy);

                   
               }
           }

           
       }
       catch(IOException ex)
       {
           ex.printStackTrace();
           return false;
       }
        
        return true;
    }
    private void closeClient(){
        try
        {
            outputStream.close();
            inputStream.close();
            clientSocket.close();            
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        clientPlayer = null;

    }

    private boolean joinThread() {
        try 
        {
            thread.join();
            return true;
        } 
        catch (InterruptedException ex) 
        {
            return false;
        }
    }
    private boolean closeObject(Closeable object)
    {
        if(object == null)
            return false;
        
        try 
        {
            object.close();
            return true;
        } 
        catch (IOException ex) 
        {
            return false;
        }
        
    }

    private void closeObjects() {
        closeClient();
        closeObject(serverSocket);
        serverPlayer = null;
    }

    private boolean interpretLoginFrame() throws IOException{
        String userLogin = inputStream.readUTF();
        
        synchronized(locker)
        {
            if(userLogin.equals(serverPlayer.getLogin()))
            {
                outputStream.writeBoolean(false);
                return false;
            }
            
            clientPlayer = new Player(idClient,userLogin);
            
            //odpowiedz serwera na probe zalogowania klienta
            
            outputStream.writeBoolean(true);
            outputStream.writeInt(clientPlayer.getId());
            
            //po zalogowaniu serwer przedstawia sie klientowi
            // wysylajac LOGIN_FRAME
            
            outputStream.writeByte(FrameType.LOGIN_FRAME);
            outputStream.writeInt(serverPlayer.getId());
            outputStream.writeUTF(serverPlayer.getLogin());
            
        }
        
        event.userLogged(clientPlayer);
        
        outputStream.writeByte(FrameType.ENEMY_TURN);
        
        return true;
        
    }

    private boolean interpretLogoutFrame() {
        
        event.userLeft(clientPlayer.getId());
        return true;
    }

    private boolean interpretBoardChangeStatusFrame() throws IOException{
        pozx = inputStream.readShort();
        pozy = inputStream.readShort();
        
        event.userReceivedShoot(serverPlayer.getId(), pozx, pozy);
        startMyTurn();
        
        return true;
    }  

    @Override
    public void isHit(boolean value,boolean isDestroyed) {
        isHit = value;
        System.out.println("Serwer isDestroyed = "+isDestroyed);
        try
        {
            
           outputStream.writeByte(FrameType.SEND_INFO_TO_PANEL);
           outputStream.writeShort((short)pozx);
           outputStream.writeShort((short)pozy);
           outputStream.writeBoolean(isHit);
           outputStream.writeBoolean(isDestroyed);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        
        
    }

    @Override
    public boolean sendInfoToPanel() {
        
        short my_pozx;
        short my_pozy;

        
        try
        {
            my_pozx = inputStream.readShort();
            my_pozy = inputStream.readShort();
            enemyHit = inputStream.readBoolean();
            enemyShipDestroyed = inputStream.readBoolean();
            
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            return false;
        }

       event.userSendsInfoToPanel(getUserId(),my_pozx,my_pozy,enemyHit,enemyShipDestroyed);
       
       if(enemyHit)
           startMyTurn();
              
       return true;
    }

    private boolean startMyTurn() {
        event.startMyTurn();
        return true;
    }

    private boolean enemySetAllAships()
    {
        event.setAbilityToShoot();
        return true;
    }

    @Override
    public boolean allShipsOnBoard() {
        try 
        {
            outputStream.writeByte(FrameType.ENEMY_SET_ALL_SHIPS);
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void enemyWonGame() 
    {
        try 
        {
            outputStream.writeByte(FrameType.ENEMY_HAS_ALREADY_WON_GAME);
        }
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
        event.userHasAlreadyLost();
    }
    private boolean interpretEnemyHasAlreadyWonGame() 
    {
       event.userHasAlreadyWon();
       return true;
    }
    
    
}
