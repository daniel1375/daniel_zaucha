
package gra;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class GameBackground{
    private  int x=2 ,x1,
                 z=0 ,z1;
    private final int ySize=1300;
    private int licznik=0;
    BufferedImage gbUp,gbDown;

    public GameBackground() throws IOException{
        gbUp = ImageIO.read(new File("pliki\\gbUP.jpg"));
        gbDown = ImageIO.read(new File("pliki\\gbDOWN.jpg"));
}
    
    public void render(Graphics2D g){
        moveImages();
        g.drawImage(gbUp,x,0,null);
        g.drawImage(gbUp,x1,0,null);
        g.drawImage(gbDown,z,510,null);
        g.drawImage(gbDown,z1,510,null);
        
    }
    public void moveImages(){
        licznik++;
        
        if(licznik%2==0){
        x--;    
        if(x < -ySize) x=0;
        x1=x+ySize; 
        }

        z--;
        if(z<-ySize) z=0;
        z1=z+ySize;
        
    }
    
}
