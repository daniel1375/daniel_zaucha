
package gra;

import static gra.GamePanel.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;


public class MyTileMap {
    private static int[][] myTab;
    private BufferedReader br;
    private BufferedImage image;
    
    static final int WIDTH=12;
    static final int HEIGHT=12;
    
    private boolean value=false;
    
    public MyTileMap(String s) {
        myTab = new int[HEIGHT][WIDTH];
        
        try{
            br = new BufferedReader(new FileReader(s));
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        for(int i=0;i<HEIGHT;i++){
            try{
                String line = br.readLine();
                String[] connect = line.split(" ");
                for(int j=0;j<WIDTH;j++){
                    myTab[i][j] = Integer.parseInt(connect[j]);            
                }
            }            
            catch(IOException ex){ 
                ex.printStackTrace();
            }
        }

    }
    
    public boolean addShip(int x,int y,int statek){
    int pozycjaX = x+1;
    int pozycjaY = y+1;
    
    for(int j=0;j<statek;j++)
        for(int k=-1;k<=1;k++)
            for(int m=-1;m<=1;m++){
                 if(myTab[pozycjaX+k][pozycjaY+j+m] > 1 || myTab[pozycjaX][pozycjaY+j] == 0){
                   setStartLocation(statek);
                   return false;
                }                  
            }
              
               
    for(int i=0;i<statek;i++){
        myTab[pozycjaX][pozycjaY+i]=statek+1;            
    }
    GamePanel.isShaking = true;
    return true;
}

    public BufferedImage loadImage(int number) {
       try{
        switch(number){ 
           
        case -1:
            return ImageIO.read(new File("pliki\\grid_shoot.jpg"));
        case -2:
            return ImageIO.read(new File("pliki\\grid_hit.jpg"));
        case -3:
            return ImageIO.read(new File("pliki\\grid_destroyed.jpg"));
        case 1:
            return ImageIO.read(new File("pliki\\jednomasztowiec.png"));
        case 2:
            return ImageIO.read(new File("pliki\\dwumasztowiec.png"));
        case 3:
            return ImageIO.read(new File("pliki\\trzymasztowiec.png"));
        case 4:
            return ImageIO.read(new File("pliki\\czteromasztowiec.png"));
        case 5:
            return ImageIO.read(new File("pliki\\pieciomasztowiec.png"));
        }
        
       }
       catch(IOException ex){
           ex.printStackTrace();
       }
       return null;
    }


    public void draw(Graphics2D g){
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {   
                  switch (myTab[i][j]) {
                      
                    case -1:
                            image = loadImage(-1);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                            break;
                    case 2:
                            image = loadImage(1);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                            break;
                    case 3:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(2);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                            
                            break;
                    case -3:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(2);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                        break;                            
                    case 4:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(3);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                        break;
                    case -4:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(3);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                        break;
                    case 5:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(4);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                        
                            break;
                    case -5:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(4);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                        break;
                    case 6:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(5);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                            break;
                    case -6:
                        if(myTab[i][j-1] <=1 && myTab[i][j-1] >=-1){
                            image = loadImage(5);
                            g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        }
                            break;
                    case -7:
                        image = loadImage(-3);
                        g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);
                        break;

            }
                  if(myTab[i][j]>=-6 && myTab[i][j]<=-2)
                  {
                         image = loadImage(-2);
                        g.drawImage(image, (j-1)*GRID_SIZE + FREE_SPACE_X, (i-1)*GRID_SIZE + FREE_SPACE_Y,null);                     
                  }
   
        }
    
        }

}

    public void myTileMapState(){
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                System.out.print(myTab[i][j]);
            }
            System.out.println();
         }
        System.out.println();
        System.out.println();
        System.out.println();
}
public static void myTabClear(int value){ 
    value++;
        for (int i = 0; i < HEIGHT; i++) 
            for (int j = 0; j < WIDTH; j++) 
                    if (myTab[i][j] == value)
                          myTab[i][j] = 1;  
}
public static int takeValueFromBoard(int tabX , int tabY)
{
    return myTab[tabX][tabY];
}

public boolean checkIfHit(int pozx, int pozy)
{
    int pozBoardX = pozx +1;
    int pozBoardY = pozy +1;
    
    
    
    if(myTab[pozBoardX][pozBoardY] > 1)
    {
      
       myTab[pozBoardX][pozBoardY] = -myTab[pozBoardX][pozBoardY];
    
       return true;   
    }
    else if( myTab[pozBoardX][pozBoardY] == 1)
    {
       
       myTab[pozBoardX][pozBoardY] = -1;
    
       return false;        
    }
    else if( myTab[pozBoardX][pozBoardY] == -1)
    {
    
       return false;        
    }
    
    return true;
    

        
}
public boolean allShipsOnBoard()
{
    int suma=0;
    

     for (int i = 0; i < HEIGHT; i++) 
            for (int j = 0; j < WIDTH; j++) 
                suma+=myTab[i][j];
     
     System.out.println("Suma statkow na planszy ="+suma);
     if(suma==169)
     {
         value = true;
         return value;
     }
     else
         return value;
}

    public boolean checkIfDestroyed(int X, int Y) 
    {
        X++;
        Y++;
        
        int getValue = myTab[X][Y];           //dla jednomasztowca bedzie -2  // dla dwumasztowca bedzie -3
        int suma = -(getValue+1)*getValue;
        int wynik=0;
        
        
        for(int i=0;i<-getValue;i++)
        {
            if(myTab[X][Y+i]<=1 && myTab[X][Y+i]>=-1)
                break;
            wynik+=myTab[X][Y+i];
            System.out.println("wynik+=myTab[X][Y+i] i="+i);
        }
        
        for(int j=1;j<-getValue;j++)
        {
            if(myTab[X][Y-j]<=1 && myTab[X][Y-j]>=-1)
                break;  
            wynik+=myTab[X][Y-j];
            System.out.println("wynik+=myTab[X][Y-j] j="+j);
        }
        
        System.out.println("Suma = "+suma);
        System.out.println("Wynik = "+wynik);
        
        if(suma != wynik)
           return false;
        
        //ustawienie ze zniszczony = -7
        
        for(int i=0;i<-getValue;i++)
        {
            if(myTab[X][Y+i]<=1 && myTab[X][Y+i]>=-1)
                break;
            myTab[X][Y+i]=-7;
        }
        
        for(int j=1;j<-getValue;j++)
        {
            if(myTab[X][Y-j]<=1 && myTab[X][Y-j]>=-1)
                break;  
            myTab[X][Y-j]=-7;
            
        }
        myTileMapState();
        
        return true;
    }
   public boolean checkIfEnemyHasAlreadyWonGame()
   {
       int suma=0;
       
                 for (int i = 0; i < MyTileMap.HEIGHT; i++) 
                    for (int j = 0; j < MyTileMap.WIDTH; j++) 
                        if(myTab[i][j]==-7)
                            suma++;
        return suma == 21;
   }
    

}