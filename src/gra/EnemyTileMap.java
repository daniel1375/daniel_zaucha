
package gra;


import static gra.GamePanel.FREE_SPACE_Y;
import static gra.GamePanel.GRID_SIZE;
import static gra.GamePanel.whichGridX;
import static gra.GamePanel.whichGridY;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;



public class EnemyTileMap
{
    private int[][] enemyTab;
    BufferedImage gridHit;
    BufferedImage gridShoot;
    BufferedImage gridDestroyed;

    public EnemyTileMap() 
    {
        enemyTab = new int[MyTileMap.WIDTH][MyTileMap.HEIGHT];
        for(int i = 0;i<MyTileMap.WIDTH;i++)
            for(int j = 0;j<MyTileMap.HEIGHT;j++)
                enemyTab[i][j] = MyTileMap.takeValueFromBoard(i, j);
        
        
        try 
        {
            gridShoot = loadImage(2);
            gridHit = loadImage(3);
            gridDestroyed = loadImage(4);
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
        
    }

    
    public boolean addShot(int x, int y,int value,boolean isDestroyed) {
        
        int pozx = x+1;
        int pozy = y+1;
        System.out.println("GamePanel.enemyTileMap.addShot , pozx = " + pozx + " pozy = " + pozy);
        enemyTab[pozx][pozy] = value;
        
        if(isDestroyed)
        {
            destroyShip(pozx, pozy, value);
            JOptionPane.showMessageDialog(null,"Zniszczyles statek przeciwnika !");
        }
            
        
        return true;
    }

 
    public BufferedImage loadImage(int liczba) throws IOException {

        switch(liczba)
        {
            case 2:
                return ImageIO.read(new File("pliki\\grid_shoot.jpg"));
                
            case 3:
                return ImageIO.read(new File("pliki\\grid_hit.jpg"));
                
            case 4:
                return ImageIO.read(new File("pliki\\grid_destroyed.jpg"));
                
            default:
                return null;
        }

    }

   
    public void draw(Graphics2D g) {
        
         for (int i = 0; i < MyTileMap.HEIGHT; i++) {
            for (int j = 0; j < MyTileMap.WIDTH; j++) 
                switch (enemyTab[i][j]) 
                {
                    case 2:
                        g.drawImage(gridShoot,(j-1) *GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_X + 600 , (i-1) * GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_Y,null);
                        break;
                    case 3:
                        g.drawImage(gridHit,(j-1) *GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_X + 600 , (i-1) * GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_Y,null);
                        break;
                    case 4:
                        g.drawImage(gridDestroyed,(j-1) *GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_X + 600 , (i-1) * GamePanel.GRID_SIZE + GamePanel.FREE_SPACE_Y,null);
                        break;
                    default:
                        break;
                }
         }
    }
    
    public void myTileMapState() {
                for (int i = 0; i < MyTileMap.HEIGHT; i++) {
                    for (int j = 0; j < MyTileMap.WIDTH; j++) {
                         System.out.print(enemyTab[i][j]);
                    }
                        System.out.println();
                }
                System.out.println();
                System.out.println();
                System.out.println();
    }
    

   private void destroyShip(int X, int Y,int value)
   {
       for(int i=0;i<5;i++)
        {
            if(enemyTab[X][Y+i] == 3)
                enemyTab[X][Y+i]=4;
            else
                break;
        }
        
        for(int j=1;j<5;j++)
        {
            if(enemyTab[X][Y-j] == 3)
                enemyTab[X][Y-j]=4;
            else
                break;  
        }
   }
   public int getValueFromBoard(int mouseX,int mouseY)
   {
       whichGridX = (mouseX - 810)/GRID_SIZE;
       whichGridY = (mouseY - FREE_SPACE_Y)/GRID_SIZE;
       whichGridX++;
       whichGridY++;
       
       System.out.println("getvaluefrom board : whichGridX=" + whichGridX +" whichGridY="+whichGridY + " enemyTab[][]="+enemyTab[whichGridX][whichGridY]);
       myTileMapState();
       return enemyTab[whichGridY][whichGridX];
   }

}
