package gra;

import static gra.MenuPanel.dataBase;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
public class RegisterPanel extends JFrame
{
    private JTextField tf_login,tf_password;
    private JButton Register;
    private JButton exit;
    private Border blackline;
    private JLabel bg;
    private GridBagConstraints gbc;
    private JPasswordField passwordField;
    
    public RegisterPanel()
    {
        
        setSize(1366,768);
        setTitle("Battle Ship Game Panel");
        
        bg = new JLabel(new ImageIcon("pliki\\background_register.jpg"));
        bg.setBounds(0,0,1366,768);
        
        bg.setLayout(new GridBagLayout());
        gbc = new GridBagConstraints();
        
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.insets = new Insets(40,40,0,0);
        gbc.ipadx = 150;
        gbc.ipady = 30;
        add(bg);
        
        createJTextField("Wpisz swoj login");
        createJPasswordField();
        createRegisterField("Zarejestruj sie");
        createExitButton();
        createJTable();
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        JOptionPane.showMessageDialog(null,"Wpisz login i haslo tworzonego konta");
    }
    public static void wyswietl_rejestracja()
    {
        new RegisterPanel();
    }
    public void createJTextField(String name)
    {
            tf_login = new JTextField(name);
       tf_login.setFont(new Font("Castellar",Font.ITALIC,15));
       tf_login.addMouseListener(new MouseAdapter() 
       { 
                   @Override
                   public void mouseClicked(MouseEvent me) 
                   { 

                       if( "Wpisz swoj nick".equals(tf_login.getText()) )
                           tf_login.setText("");
                   }    
            });

       bg.add(tf_login,gbc);
    }
    public void createJPasswordField()
    {
        passwordField = new JPasswordField(10);
        passwordField.setBorder(blackline);

        bg.add(passwordField,gbc);
    }
    public void createRegisterField(String name)
    {
        Register = new JButton(name);
        Register.setFont(new Font("Castellar", Font.BOLD, 20));
        Register.setBackground(Color.CYAN);
        Register.setFocusPainted(false);
        Register.addMouseListener(new MouseAdapter() 
        { 
            @Override
                    public void mouseEntered(MouseEvent me) 
                    { 
                       Register.setBackground(Color.GRAY);                                  
                    } 
            @Override
                    public void mouseExited(MouseEvent me) 
                    { 
                       Register.setBackground(Color.CYAN);
                       Register.setBorder(blackline); 
                    }
            @Override
                    public void mouseClicked(MouseEvent me)
                    {  
                        if(passwordField.getPassword().length<5)
                        {
                            JOptionPane.showMessageDialog(null,"Wprowadz dluzsze haslo do konta");
                            return;
                        }
                        dataBase.addUserToDataBase(tf_login.getText(), passwordField.getPassword());

                    }

             });

        bg.add(Register,gbc);
    }
    public void createJTable()
    {
        
        DefaultTableModel model = dataBase.createTableModel();
              
        int rows = dataBase.getNumberOfRows();
        
        gbc.ipadx = 200;
        if(rows>8)
            rows=8;
            
        gbc.ipady = rows*16;
      
        
        JTable table = new JTable();
        table.setModel(model);
        bg.add(new JScrollPane(table),gbc);
        
    }
    public void createExitButton()
    {
        exit = new JButton("MENU");
        exit.setFont(new Font("Castellar", Font.BOLD, 20));
        exit.setBackground(Color.CYAN);
        exit.setFocusPainted(false);
        exit.addMouseListener(new MouseAdapter() 
        { 
            @Override
                    public void mouseEntered(MouseEvent me) 
                    { 
                       exit.setBackground(Color.GRAY);                                  
                    } 
            @Override
                    public void mouseExited(MouseEvent me) 
                    { 
                       exit.setBackground(Color.CYAN);
                       exit.setBorder(blackline); 
                    }
            @Override
                    public void mouseClicked(MouseEvent me)
                    {  
                        dispose();

                    }

             });

        bg.add(exit,gbc);

    }

}
