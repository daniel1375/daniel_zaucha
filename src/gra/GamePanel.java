package gra;

import static gra.MenuPanel.dataBase;
import interfacesAndOthers.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;



public class GamePanel extends JPanel implements Runnable , MouseListener , MouseMotionListener
{
    //dimensions
    private static final int WIDTH = 1300;
    private static final int HEIGHT = 700;
    public static final int GRID_SIZE = 30;
    public static int whichGridX;
    public static int whichGridY;
    public static final int FREE_SPACE_X = 210;
    public static final int FREE_SPACE_Y = 120;
    
    
    
    public Communicator communicator;
    public final int ID;
    
    //JFrame
    private JFrame okno ;
    private JButton endGame;
    
    //image
    private BufferedImage image;
    
    //game thread
    private Thread thread;
    private boolean running = false;
    private static final int WAIT = 1;
    
    //tilemap
    private WorldsTileMap worldsTileMap;
    private Graphics2D g;
    private MyTileMap myTileMap;
    public EnemyTileMap enemyTab;
    
    //background
    GameBackground gb ;
    
    //array that contains ships
    public static ArrayList<CreateShip> ShipArray;
   
    
    Random shakingEffect;
    public static boolean isShaking;
    private boolean jPanelStatus = false;
    private boolean enemySetAllShipsOnBoard = false;
    private boolean statusBoard = false;

    
    public GamePanel(int id ,Communicator communicator)
    {
        super();
        this.ID = id;
        this.communicator = communicator;
        okno = new JFrame("BattleShip game panel");
        endGame=new JButton("Zakoncz gre !");
        endGame.setBounds(500, 600, 100, 100);
        endGame.setVisible(true);
        add(endGame);
        setPreferredSize(new Dimension(WIDTH ,HEIGHT));
        okno.add(this);
        okno.setBounds(30,15,WIDTH,HEIGHT);
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
        okno.setResizable(false);
        requestFocus();
        setLayout(null);
   
    }

    
    public void init(){
        running = true;
        image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
        g = (Graphics2D) image.getGraphics();
        
        worldsTileMap = new WorldsTileMap("pliki\\map.txt");
        myTileMap = new MyTileMap("pliki\\my_tab.txt");
        enemyTab = new EnemyTileMap();
        ShipArray = new ArrayList<CreateShip>();
        
        makeShips(3,16,1);                          //setting start positions of ships
        makeShips(5,16,2);
        makeShips(8,16,3);
        makeShips(12,16,4);
        makeShips(17,16,5);
        
        makeShips(7,18,1);
        makeShips(9,18,2);
        makeShips(12,18,3);
        
        try{
            gb = new GameBackground();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        shakingEffect = new Random();
    }
    
    @Override
    public void addNotify(){
        super.addNotify();
        if(thread == null){
            thread = new Thread(this);
            thread.start();
            addMouseListener(this);
            addMouseMotionListener(this);
        }
    }
    @Override
    public void run() {
        init();
        while(running){

            gameRender();
            gameDraw();
            try{
                thread.sleep(WAIT);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    private void gameRender(){
            gb.render(g);
            worldsTileMap.draw(g);
            myTileMap.draw(g);
            enemyTab.draw(g);
            for(CreateShip ship : ShipArray)
                ship.draw(g);
    }
private void gameDraw(){
         int pozx =okno.getX();
         int pozy = okno.getY();
         
        Graphics2D g2 = (Graphics2D)this.getGraphics();
        if(isShaking)
        {
           double start = System.nanoTime()/1000000000;
           double end;
           while(true)
           {

               okno.setLocation(pozx+ shakingEffect.nextInt(10), pozy+shakingEffect.nextInt(10));
               end = System.nanoTime()/1000000000;
               if((end - start) > 0.5) break;                
           }
                          
        }

        
        isShaking = false;
        g2.drawImage(image,0,0,null);
        g2.dispose();
    }
    private void makeShips(int i,int j,int statek){
        ShipArray.add(new CreateShip(g,statek,i*GRID_SIZE,j*GRID_SIZE));
    }
    private boolean addShipsToBoard(int mouseX, int mouseY,int which){
        
            whichGridX = (mouseX - FREE_SPACE_X)/GRID_SIZE;
            whichGridY = (mouseY - FREE_SPACE_Y)/GRID_SIZE;       
        
            
        if(worldsTileMap.boardRectangle1.contains(mouseX,mouseY) && myTileMap.addShip(whichGridY, whichGridX, which)){
            
        if(myTileMap.allShipsOnBoard())
        {
            communicator.allShipsOnBoard();
        }
            
            myTileMap.draw(g);
            return true;
        }
        else
        {
            setStartLocation(which);            
            return false;
        }
        
        
    }
    
    private boolean setShotToEnemy(int mouseX , int mouseY)
    {
        if(!myTileMap.allShipsOnBoard())
        {
            JOptionPane.showMessageDialog(null,"Nie mozesz oddac strzalu , nie ustawiles wszystkich statkow" );
            return false;
        }
            
        if(!enemySetAllShipsOnBoard)
        {
            JOptionPane.showMessageDialog(null,"Poczekaj , przeciwnik jeszcze nie ustawił wszystkich statków !" );
            return false;
        }        
        
        if (!jPanelStatus) 
        {
            JOptionPane.showMessageDialog(null,"Poczekaj , tura przeciwnika" );
            return false;
        }
        jPanelStatus = false;
        

        
            whichGridX = (mouseX - 810)/GRID_SIZE;
            whichGridY = (mouseY - FREE_SPACE_Y)/GRID_SIZE;
            
            System.out.println(" SetShotToEnemy  , whichGridX:" + whichGridX + "  whichGridY : " + whichGridY);
            
            communicator.sendSetShootToEnemy(whichGridY, whichGridX);    
                       
            return true;
        
    }
    public static void setStartLocation(int whichShip){
        
               switch(whichShip){
                   case 1:
                       
                           
                           
                       
                           ShipArray.get(0).changeLocation(GRID_SIZE*3, GRID_SIZE*16);
                           ShipArray.get(0).setIsOnBoard(false);
                           
                           ShipArray.get(5).changeLocation(GRID_SIZE*7, GRID_SIZE*18);
                           ShipArray.get(5).setIsOnBoard(false);                           
                        
                            MyTileMap.myTabClear(whichShip);
                       break;
                   case 2:
                           
                           
                           ShipArray.get(1).changeLocation(GRID_SIZE*5, GRID_SIZE*16);
                           ShipArray.get(1).setIsOnBoard(false);
 
                           ShipArray.get(6).changeLocation(GRID_SIZE*9, GRID_SIZE*18);
                           ShipArray.get(6).setIsOnBoard(false);
                           
                           MyTileMap.myTabClear(whichShip);                           
                       break;
                   case 3:
                           
                           
                           ShipArray.get(2).changeLocation(GRID_SIZE*7, GRID_SIZE*16);
                           ShipArray.get(2).setIsOnBoard(false);
                           
                           ShipArray.get(7).changeLocation(GRID_SIZE*12, GRID_SIZE*18);
                           ShipArray.get(7).setIsOnBoard(false);

                           MyTileMap.myTabClear(whichShip);                           
                                                  
                       break;
                   case 4:
                            
                            
                            ShipArray.get(3).changeLocation(GRID_SIZE*12, GRID_SIZE*16);
                            ShipArray.get(3).setIsOnBoard(false);
                            
                       break;
                   case 5:
                            
                            
                            ShipArray.get(4).changeLocation(GRID_SIZE*17, GRID_SIZE*16);
                            ShipArray.get(4).setIsOnBoard(false);
                            
                       break;          
               }
    }
    public int getID()
    {
        return ID;
    }
    public boolean checkIfHit(int pozx , int pozy)
    {
        System.out.println("CheckIfHit dla pozx:" + pozx + "  pozy : " + pozy);
         if (myTileMap.checkIfHit(pozx, pozy))
         {
             if(!myTileMap.checkIfDestroyed(pozx, pozy))
                communicator.isHit(true,false);
             else
             {
                 communicator.isHit(true, true);
                 if(myTileMap.checkIfEnemyHasAlreadyWonGame())
                     communicator.enemyWonGame();
                     
             }
                
             
              return true;
         }
         else
         {
             communicator.isHit(false,false);
             System.out.println("komunikator ustawil isHit = false");
             return false;
         }
            
    }

    public void setStatusOfJPanel(boolean value)
    {
        jPanelStatus = value;
    }
    public void setEnemySetAllShipsOnBoard(boolean value)
    {
        enemySetAllShipsOnBoard = value;
    }
    public void userHasAlreadyWongame()
    {
        JOptionPane.showMessageDialog(null, "Zniszczyles wszystkie statki przeciwnika, Wygrales !");
        dataBase.setUserWon(communicator.getUserLogin());
        endGame();
    }        
    
    @Override
    public void mouseClicked(MouseEvent me) {

        if(worldsTileMap.boardRectangle2.contains(me.getX(),me.getY()) && enemyTab.getValueFromBoard(me.getX(),me.getY()) == 1)
        {
            setShotToEnemy(me.getX(), me.getY());
            enemyTab.draw(g);
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

        
        for(CreateShip ship : ShipArray)
           if(ship.rect.contains(me.getX(),me.getY()) && !ship.getIsOnBoard()){
               ship.setStatusFocus(true);
            }

               
    }
    @Override
    public void mouseReleased(MouseEvent me) {

        
        for(CreateShip ship : ShipArray)
           if(ship.getStatusFocus()){
               if(addShipsToBoard(me.getX(),me.getY(),ship.getShip()))
                   ship.setIsOnBoard(true);
               else
                   ship.setIsOnBoard(false);
               
               ship.setStatusFocus(false);
               
            }

    }

    @Override
    public void mouseEntered(MouseEvent me) {
       
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        

        for(CreateShip ship : ShipArray)
           if(ship.getStatusFocus() && !ship.getIsOnBoard()){
               ship.changeLocation(me.getX()- (GRID_SIZE/2),me.getY()-(GRID_SIZE/2));
               ship.draw(g);
           }
    }

    @Override
    public void mouseMoved(MouseEvent me) {
 
    }
    
    public void endGame() 
    {
       running = false;
       okno.dispose();
       MenuPanel.wyswietl();
    }

    public void userHasAlreadyLostgame() 
    {
        JOptionPane.showMessageDialog(null, "Twoje wszystkie statki zostaly zniszczone , Przegrales !");
        dataBase.setUserLost(communicator.getUserLogin());
        endGame();
    }
}