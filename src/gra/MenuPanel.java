package gra;

import interfacesAndOthers.Connector;
import Klient.Client;
import Management.DataBase;
import Serwer.Server;
import java.awt.*;
import javax.swing.*;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.border.Border;
import static gra.RegisterPanel.wyswietl_rejestracja; 
import java.util.Random;

public class MenuPanel extends JFrame implements MouseListener 
{
    JButton bPlay,bLogin,howToPlay,Exit,Register;
    Border blackline,cyanline;
    JTextField tf_login,tf_password;
    JCheckBox server , client;
    JLabel tlo;
    GridBagConstraints gbc;
    Random random = new Random();
    public static DataBase dataBase;
    
    int idServer , idClient;
    private final JPasswordField passwordField;


    public MenuPanel() throws IOException 
    {
    dataBase = new DataBase();
    idServer =random.nextInt(100); 
    idClient = random.nextInt(100);
    tlo = new JLabel(new ImageIcon("pliki\\background.jpg"));
    blackline = BorderFactory.createLineBorder(Color.BLACK);
    cyanline = BorderFactory.createLineBorder(Color.CYAN);
    
    tlo.setLayout(new GridBagLayout());                            //ustawienie layoutu na gridbaglayout
    gbc = new GridBagConstraints();          //tworzenie ogarnicznika
    
    gbc.insets = new Insets(40,400,0,0);                          //kazda komorka jest co 40 pixeli
    gbc.gridwidth = GridBagConstraints.REMAINDER;           
    gbc.ipadx = 40;                                             //ustawnie rozmiarow komorki
    gbc.ipady = 20; 
    
    serverAndClientBox();

    tlo.setBounds(0, 0, 1366, 768);
    add(tlo);
    setSize(1366,768);
    setTitle("Battle Ship");
    
    tf_login = new JTextField("login");
    tf_login.setFont(new Font("Castellar",Font.ITALIC,15));
    tf_login.setBorder(blackline);
    tf_login.addMouseListener(new MouseAdapter() 
    { 
                @Override
                public void mouseClicked(MouseEvent me) 
                { 
                    if( "login".equals(tf_login.getText()) )
                        tf_login.setText("");
                }    
         });
    
    tlo.add(tf_login,gbc);
    
    passwordField = new JPasswordField(10);
    passwordField.setBorder(blackline);
    
    tlo.add(passwordField,gbc);
    
    bLogin = new JButton("Zaloguj sie i GRAJ");
    bLogin.setFont(new Font("Castellar", Font.BOLD, 20));
    bLogin.setBackground(Color.CYAN);
    bLogin.setFocusPainted(false);
    bLogin.setBorder(blackline);
    bLogin.addMouseListener(new MouseAdapter() 
    { 
                @Override
                public void mouseEntered(MouseEvent me) 
                { 
                    bLogin.setBackground(Color.GRAY);
                    bLogin.setBorder(cyanline);                
                } 
                @Override
                public void mouseExited(MouseEvent me) 
                { 
                    bLogin.setBackground(Color.CYAN);
                    bLogin.setBorder(blackline); 
                }
                   @Override
                public void mouseClicked(MouseEvent me) 
                {
                    
                    if(dataBase.loginWithAccount(tf_login.getText(), passwordField.getPassword())!=null)
                    {
                        if( server.isSelected() && !client.isSelected())
                        {
                            dispose();
                            
                            String userLogin = tf_login.getText();
                            
                            Server server = new Server(idServer,idClient);
        
                            Connector.eventManager("Server",idServer, server);
        
                            server.runServer(5000,userLogin);
                            
                            
                        }
                        else if( !server.isSelected() && client.isSelected())
                        {
                            dispose(); 
                            
                            String userLogin = tf_login.getText();
                            
                            Client client = new Client();
                            
                            Connector.eventManager("Client",idClient, client );

                            client.connect("127.0.0.1",5000,userLogin);  
                            
                                                                     
                        }
                        else if(!server.isSelected() && !client.isSelected())
                            JOptionPane.showMessageDialog(null,"Wybierz klient lub serwer");
                    }

                } 
                
         });

    tlo.add(bLogin,gbc);
    
    Register = new JButton("Rejestracja && wyniki");
    Register.setFont(new Font("Castellar", Font.BOLD, 20));
    Register.setBackground(Color.CYAN);
    Register.setFocusPainted(false);
    Register.setBorder(blackline);
    Register.addMouseListener(new MouseAdapter() 
    { 
        @Override
                public void mouseEntered(MouseEvent me) 
                { 
                   Register.setBackground(Color.GRAY);
                   Register.setBorder(cyanline);                
                } 
        @Override
                public void mouseExited(MouseEvent me) 
                { 
                   Register.setBackground(Color.CYAN);
                   Register.setBorder(blackline); 
                }
        @Override
                public void mouseClicked(MouseEvent me)
                {
                   wyswietl_rejestracja();
                }
                
         });
    
    tlo.add(Register,gbc);
    
    howToPlay = new JButton("Niewiesz jak grac? Kliknij tutaj!");
    howToPlay.setFont(new Font("Castellar", Font.BOLD, 20));
    howToPlay.setBackground(Color.CYAN);
    howToPlay.setFocusPainted(false); 
    howToPlay.setBorder(blackline);
    howToPlay.addMouseListener(new MouseAdapter() 
    { 
                @Override
                public void mouseEntered(MouseEvent me) 
                { 
                    howToPlay.setBackground(Color.GRAY);
                    howToPlay.setBorder(cyanline);                
                } 
                @Override
                public void mouseExited(MouseEvent me) 
                { 
                    howToPlay.setBackground(Color.CYAN);
                    howToPlay.setBorder(blackline); 
                } 
                
                @Override
                public void mouseClicked(MouseEvent me) 
                {
                    Runtime rt = Runtime.getRuntime();
                    String url = "http://www.zasadygry.pl/statki_zasady_gry_12_1.htm";
                    try 
                    {
                        rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
                    } 
                    catch (IOException ex) 
                    {
                        Logger.getLogger(MenuPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } 
                
                
         });

    tlo.add(howToPlay,gbc);
    
    Exit = new JButton("WYJSCIE");
    Exit.setFont(new Font("Castellar", Font.BOLD, 20));
    Exit.setBackground(Color.CYAN);
    Exit.setFocusPainted(false);
    Exit.setBorder(blackline);
    Exit.addMouseListener(new MouseAdapter() 
    { 
        @Override
                public void mouseEntered(MouseEvent me) 
                { 
                   Exit.setBackground(Color.GRAY);
                   Exit.setBorder(cyanline);                
                } 
        @Override
                public void mouseExited(MouseEvent me) 
                { 
                   Exit.setBackground(Color.CYAN);
                   Exit.setBorder(blackline); 
                }
        @Override
                public void mouseClicked(MouseEvent me)
                {
                    dispose();
                }
                
         });
    
    tlo.add(Exit,gbc);
    
    }
    
    public static void wyswietl()
    {
        MenuPanel okienko;
        try 
        {
            okienko = new MenuPanel();
            okienko.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            okienko.setVisible(true);
        } 
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        
                
    }
   
    @Override
    public void mouseClicked(MouseEvent me) 
    {
        
    }

    @Override
    public void mousePressed(MouseEvent me) 
    {
       
    }

    @Override
    public void mouseReleased(MouseEvent me) 
    {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) 
    {
        dispose();
    }

    @Override
    public void mouseExited(MouseEvent me) 
    {
        
    }
    private void serverAndClientBox(){
        boolean value1 = false, value2 = false;
        
        server = new JCheckBox ( " Run as Server " , value1);
        client = new JCheckBox(" Run as Client" , value2);
        server.setFont(new Font("Times New Roman",Font.ITALIC,20));
        server.setBorder(blackline);
        server.setOpaque(false);
        server.setFocusPainted(false);

        client.setFont(new Font("Times New Roman",Font.ITALIC,20));
        client.setBorder(blackline);
        client.setOpaque(false);
        client.setFocusPainted(false);
        tlo.add(server,gbc);
        tlo.add(client,gbc);
        }
    
}
