
package gra;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class CreateShip {
   private BufferedImage ship;
   public Rectangle rect;
   private boolean isFocused;
   private boolean isOnBoard;
   private boolean lastSelected;
   private final int WHICH;
   private int X;
   private int Y;
    
    CreateShip(Graphics2D g,int which,int x,int y){
        rect = new Rectangle(x,y,30*which,30);
        this.WHICH = which;
        this.X = x;
        this.Y = y;
        try{
        loadImage(this.WHICH);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    private void loadImage(int number) throws IOException{
       
        switch(number){ 
           
        case 1:
            ship = ImageIO.read(new File("pliki\\jednomasztowiec.png"));
            break;
        case 2:
            ship = ImageIO.read(new File("pliki\\dwumasztowiec.png"));
            break;
        case 3:
            ship = ImageIO.read(new File("pliki\\trzymasztowiec.png"));
            break;
        case 4:
            ship = ImageIO.read(new File("pliki\\czteromasztowiec.png"));
            break;
        case 5:
            ship = ImageIO.read(new File("pliki\\pieciomasztowiec.png"));
            break;
        }
    }
        
    
    public void draw(Graphics2D g){
       if(!isOnBoard) 
        g.drawImage(ship,X,Y,null);
    }
    public void changeLocation(int pozx,int pozy){
        X = pozx;
        Y = pozy;
        rect.setLocation(X, Y);
    }
    public int getShip(){
        return WHICH;
    }
    public boolean getStatusFocus(){
        return isFocused;
    }
    public void setStatusFocus(boolean value){
        isFocused = value;
    }
    public void setIsOnBoard(boolean value){
        isOnBoard = value;
    }
    public boolean getIsOnBoard(){
        return isOnBoard;
    }
    public boolean getLastSelected(){
        return lastSelected;
    }
    public void setLastSelected(boolean value){
        lastSelected = value;
    }
}
