package gra;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class WorldsTileMap {

    private int mapWidth;
    private int mapHeight;

    private int[][] tileMap;
    private BufferedImage gridEmpty;
    private BufferedImage gridHit;
    private BufferedImage marking;
    
    public Rectangle boardRectangle1;
    public Rectangle boardRectangle2;
    
    private Point boardPosition1;
    private Point boardPosition2;
    private int counter=0;

    
    WorldsTileMap(String s) {
        super();
        try {
            boardPosition1 = new Point();
            boardPosition2 = new Point();
            gridEmpty = ImageIO.read(new File("pliki\\grid_empty.jpg"));
            marking = ImageIO.read(new File("pliki\\oznakowanie.png"));
            BufferedReader br = new BufferedReader(new FileReader(s));
            mapWidth = Integer.parseInt(br.readLine());
            mapHeight = Integer.parseInt(br.readLine());
            tileMap = new int[mapHeight][mapWidth];
            

            for (int i = 0; i < mapHeight; i++) {
                String line = br.readLine();
                String[] tab_numbers = line.split(" ");

                for (int j = 0; j < mapWidth; j++) {
                    tileMap[i][j] = Integer.parseInt(tab_numbers[j]);
                    if(tileMap[i][j] == 1){
                        if(counter == 0)
                        {
                            boardPosition1.x = 30*j;
                            boardPosition1.y = 30*i;                           
                        }
                        if(counter == 10)
                        {
                            boardPosition2.x = 30*j;
                            boardPosition2.y = 30*i;
                        }
                        counter ++;

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        boardRectangle1 = new Rectangle(boardPosition1.x,boardPosition1.y,300,300);
        boardRectangle2 = new Rectangle(boardPosition2.x,boardPosition2.y,300,300);
       
    }

    public void draw(Graphics2D g) {
        for (int i = 0; i < mapHeight; i++) {
            for (int j = 0; j < mapWidth; j++) {

                switch (tileMap[i][j]) {
                    case 0:
                        continue;
                    case 1:
                        g.drawImage(gridEmpty, j * 30, i * 30, null);       // dodac do drugiej tablicy dwuwymiarowej id rysowanego elementu dzieki temu nie bedziemy uzywac kontrolek z ifami
                        break;
                    case 9:
                         g.drawImage(marking,j*30,i*30,null);
                        break;
                }
            }
        }
    }

    public void update() {

    }
}
