
package interfacesAndOthers;

public interface Communicator 
{
	public int getUserId();
	public String getUserLogin();
	
	public void setEvent(GamerEvent event);
        
	public boolean sendSetShootToEnemy(int pozx,int pozy);
        
        public void isHit(boolean value,boolean isDestroyed);
       
        public boolean sendInfoToPanel();
        
        public boolean allShipsOnBoard();
        
        public void enemyWonGame();


}