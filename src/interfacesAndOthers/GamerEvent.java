
package interfacesAndOthers;

import Serwer.Player;

public interface GamerEvent
{
	public void connectionError();
	
	public void userLogged( Player player );
	
	public void userLeft( int id );
        
        public void userReceivedShoot( int userId, int pozx , int pozy);
       
	public void userSendsInfoToPanel(int id,short pozx,short pozy,boolean value,boolean isDestroyed);

        public void startMyTurn();

        public void setAbilityToShoot();
        
        public void userHasAlreadyWon();
        
        public void userHasAlreadyLost();

        public void endGame();
}
