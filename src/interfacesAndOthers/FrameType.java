
package interfacesAndOthers;

public class FrameType 
{
	public static final int LOGIN_FRAME = 1;
	public static final int LOGOUT_FRAME = 2;
	public static final int BOARD_CHANGE_STATUS_FRAME = 3;
        public static final int SEND_INFO_TO_PANEL =4;
        public static final int ENEMY_TURN =5;
        public static final int ENEMY_SET_ALL_SHIPS=6;
        public static final int ENEMY_HAS_ALREADY_WON_GAME=7;

}
