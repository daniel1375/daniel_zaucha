package interfacesAndOthers;

import Serwer.Player;


public interface PlayerObserver
{
	public boolean registerPlayer( Player player);
	
	public void deregisterPlayer( int id );
	
        
        public boolean receiveShot( int id, short pozx, short pozy );
        
        public void setReceivedInfoToPanel(int id,short pozx,short pozy,boolean enemyHit,boolean isDestroyed);
	
	public void clearPlayers();
}
