
package interfacesAndOthers;

import Management.ManagementPanel;
import Serwer.Player;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class Connector
{
 
    public static void eventManager(final String name ,final int id ,final Communicator communicator)
    {
        ManagementPanel panel = new ManagementPanel(id,communicator);

        
        communicator.setEvent(new GamerEvent()
        {
            @Override
            public void connectionError() {
                SwingUtilities.invokeLater(() -> 
                        {
                                JOptionPane.showMessageDialog(null, "Connection Error !");
                                panel.clearPlayers();
                        }
                    );
                
            }

            @Override
            public void userLogged(Player player) {
                SwingUtilities.invokeLater(( ) -> panel.registerPlayer(player));
                System.out.println( "User logged! id: " +player.getId()  + " login: " + player.getLogin() );
            }

            @Override
            public void userLeft(int id) {
                SwingUtilities.invokeLater(( ) -> panel.deregisterPlayer(id));
                System.out.println( "User left! id: " + id);
            }


            @Override
            public void userReceivedShoot(int userId, int pozx, int pozy) {
                SwingUtilities.invokeLater( ( ) -> panel.receiveShot(userId,(short)pozx,(short)pozy));
            }

            @Override
            public void userSendsInfoToPanel(int userId,short pozx,short pozy,boolean value,boolean isDestroyed) {
                SwingUtilities.invokeLater( ( ) -> panel.setReceivedInfoToPanel(userId,pozx,pozy,value,isDestroyed));
            }

            @Override
            public void startMyTurn() {
                SwingUtilities.invokeLater( ( ) -> panel.startMyTurn());
                
            }

            @Override
            public void setAbilityToShoot() {
                 SwingUtilities.invokeLater( ( ) -> panel.setAbilityToShoot());
            }

            @Override
            public void userHasAlreadyWon() {
                SwingUtilities.invokeLater(()-> panel.InfoToPanelUserHasAlreadyWonGame());
            }

            @Override
            public void endGame() {
                SwingUtilities.invokeLater(()-> panel.endGame());
            }

            @Override
            public void userHasAlreadyLost() {
                SwingUtilities.invokeLater(()-> panel.InfoToPanelUserHasAlreadyLostGame());
            }
            
            
            
            
        });
        
    }
}
